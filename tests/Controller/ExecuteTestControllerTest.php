<?php

namespace Drupal\drulenium\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the drulenium module.
 */
class ExecuteTestControllerTest extends WebTestBase {


  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => "drulenium ExecuteTestController's controller functionality",
      'description' => 'Test Unit for module drulenium and controller ExecuteTestController.',
      'group' => 'Other',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests drulenium functionality.
   */
  public function testExecuteTestController() {
    // Check that the basic functions of module drulenium.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}
