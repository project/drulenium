(function ($, Drupal) {
  Drupal.behaviors.drulenium = {
    /**
     * PhotoSwipe Options, coming from Drupal.settings.
     */
    druleniumOptions: {},
    /**
     * Load PhotoSwipe once page is ready
     */
    attach: function (context, settings) {
      //console.log("attach");
      //console.log(context);
      //console.log( $.cookie("example") );
      this.druleniumOptions = settings.drulenium ? settings.drulenium.options : {};
      console.log(this.druleniumOptions);
    },
    takeScreenshot: function () {
      console.log("takeScreenshot");
      html2canvas(document.querySelector("body")).then(canvas => {
        //console.log("canvas");
        //console.log(canvas);
        //document.querySelector("#content").appendChild(canvas)
        //var image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");  // here is the most important part because if you dont replace you will get a DOM 18 exception.
        //window.location.href=image; // it will save locally
        var dataURL = canvas.toDataURL();
        var data = JSON.stringify({
          message: 'Hello World',
          imgBase64: dataURL
        });
        $.ajax({
          type: "GET",
          url: "/session/token"
        }).done(function(token) {
          console.log('token'); 
          console.log(token);
          $.ajax({
	          type: "POST",
	          url: "/drulenium/savescreenshot",
	          "headers": {
	            "Content-Type": "application/json",
	            "Accept": 'application/json',
	            "X-CSRF-Token": token,
	          },
	          data: data
	        }).done(function(o) {
	          console.log('saved');
	          //console.log(o); 
	        });
        });
      });
    },

  };
  $(document).ready(function () {
    console.log("ready");

    var nextPage = Drupal.behaviors.drulenium.druleniumOptions.nextPage;
    console.log(nextPage);

    Drupal.behaviors.drulenium.takeScreenshot();
    /*setTimeout(function() {
      goToNext(nextPage);
    }, 4000);*/
  });
})(jQuery, Drupal);

function goToNext(nextPage) {
  console.log("goToNext");
  if(nextPage){
    window.location.replace(nextPage+"?testMode=true");
  }
}

