<?php

/**
 * @file
 * Contains drulenium_entity.page.inc.
 *
 * Page callback for Drulenium entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Drulenium entity templates.
 *
 * Default template: drulenium_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_drulenium_entity(array &$variables) {
  // Fetch DruleniumEntity Entity Object.
  $drulenium_entity = $variables['elements']['#drulenium_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
