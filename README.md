INSTALLATION
------------
"repositories": [
        {
            "type": "composer",
            "url": "https://packages.drupal.org/8"
        },
        {
        "type": "package",
        "package": {
          "name": "niklasvh/html2canvas",
          "version": "1.0.0",
          "type": "drupal-library",
          "dist": {
            "url": "https://github.com/niklasvh/html2canvas/releases/download/v1.0.0-rc.5/html2canvas.min.js",
            "type": "file"
          }
        }
      }
    ],
"installer-paths": {
            "core": ["type:drupal-core"],
            "libraries/{$name}": ["type:drupal-library", "niklasvh/html2canvas"],
            "modules/contrib/{$name}": ["type:drupal-module"],
 * Install as usual, see
   https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8 for further
   information.

CONFIGURATION
-------------
