<?php

namespace Drupal\drulenium;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Drulenium entity entities.
 *
 * @ingroup drulenium
 */
class DruleniumEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Drulenium entity ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\drulenium\Entity\DruleniumEntity $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.drulenium_entity.edit_form',
      ['drulenium_entity' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
