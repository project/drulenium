<?php

namespace Drupal\drulenium\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class ExecuteTestController.
 */
class ExecuteTestController extends ControllerBase {

  /**
   * Runtest.
   *
   * @return string
   *   Return Hello string.
   */
  public function runTest() {
    $build['myele1'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: runTest1')
    ];
    //$build['myele']['#attached']['library'][] = 'drulenium/html2canvas';
    $build['myele']['#attached']['library'][] = 'drulenium/tests.init';
    $options = array(
		"path" => "node",
    );
    $build['myele']['#attached']['drupalSettings']['drulenium']['options'] = $options;
    $build['myele2'] = [
      '#type' => 'markup',
      '#markup' => '<iframe id="myframe" src="http://drulenium.dd:8083/node"></iframe>',
    ];
    return $build;
  }

}
