<?php

namespace Drupal\drulenium\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Drulenium entity entities.
 *
 * @ingroup drulenium
 */
class DruleniumEntityDeleteForm extends ContentEntityDeleteForm {


}
