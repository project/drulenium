<?php

namespace Drupal\drulenium\Plugin\rest\resource;

use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Serialization\Json;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "save_screenshot_rest_resource",
 *   label = @Translation("Save screenshot rest resource"),
 *   uri_paths = {
 *     "create" = "/drulenium/savescreenshot"
 *   }
 * )
 */
class SaveScreenshotRestResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.factory')->get('drulenium');
    $instance->currentUser = $container->get('current_user');
    return $instance;
  }

    /**
     * Responds to POST requests.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *   The current request.
     *
     * @return \Drupal\rest\ModifiedResourceResponse
     *   The HTTP response object.
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     *   Throws exception expected.
     */
    public function post(Request $request) {

        // You must to implement the logic of your REST Resource here.
        // Use current user after pass authentication to validate access.
        if (!$this->currentUser->hasPermission('access content')) {
            throw new AccessDeniedHttpException();
        }
        $encoded_payload = $request->getContent();
        $payload = Json::decode($encoded_payload);
		\Drupal::logger('drulenium1')->notice(print_r($payload, true));
		\Drupal::logger('drulenium2')->notice($payload["imgBase64"]);
		$data = $payload["imgBase64"];
		$exploded = explode(',', $data, 2); // limit to 2 parts, i.e: find the first comma
		$encoded = $exploded[1]; // pick up the 2nd part
		$decoded = base64_decode($encoded);
		$im = imagecreatefromstring($decoded);
		if ($im !== false) {
		  \Drupal::logger('drulenium')->notice("im not false");
		  imagepng($im, "sites/default/files/abc.png");
		}
        return new ModifiedResourceResponse($payload, 200);
    }

}
