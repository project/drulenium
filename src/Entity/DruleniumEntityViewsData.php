<?php

namespace Drupal\drulenium\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Drulenium entity entities.
 */
class DruleniumEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
