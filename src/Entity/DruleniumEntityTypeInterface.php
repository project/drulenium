<?php

namespace Drupal\drulenium\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Drulenium entity type entities.
 */
interface DruleniumEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
