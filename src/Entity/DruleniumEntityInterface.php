<?php

namespace Drupal\drulenium\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Drulenium entity entities.
 *
 * @ingroup drulenium
 */
interface DruleniumEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Drulenium entity name.
   *
   * @return string
   *   Name of the Drulenium entity.
   */
  public function getName();

  /**
   * Sets the Drulenium entity name.
   *
   * @param string $name
   *   The Drulenium entity name.
   *
   * @return \Drupal\drulenium\Entity\DruleniumEntityInterface
   *   The called Drulenium entity entity.
   */
  public function setName($name);

  /**
   * Gets the Drulenium entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Drulenium entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Drulenium entity creation timestamp.
   *
   * @param int $timestamp
   *   The Drulenium entity creation timestamp.
   *
   * @return \Drupal\drulenium\Entity\DruleniumEntityInterface
   *   The called Drulenium entity entity.
   */
  public function setCreatedTime($timestamp);

}
