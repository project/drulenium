<?php

namespace Drupal\drulenium\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Drulenium entity type entity.
 *
 * @ConfigEntityType(
 *   id = "drulenium_entity_type",
 *   label = @Translation("Drulenium entity type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\drulenium\DruleniumEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\drulenium\Form\DruleniumEntityTypeForm",
 *       "edit" = "Drupal\drulenium\Form\DruleniumEntityTypeForm",
 *       "delete" = "Drupal\drulenium\Form\DruleniumEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\drulenium\DruleniumEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "drulenium_entity_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "drulenium_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/drulenium_entity_type/{drulenium_entity_type}",
 *     "add-form" = "/admin/structure/drulenium_entity_type/add",
 *     "edit-form" = "/admin/structure/drulenium_entity_type/{drulenium_entity_type}/edit",
 *     "delete-form" = "/admin/structure/drulenium_entity_type/{drulenium_entity_type}/delete",
 *     "collection" = "/admin/structure/drulenium_entity_type"
 *   }
 * )
 */
class DruleniumEntityType extends ConfigEntityBundleBase implements DruleniumEntityTypeInterface {

  /**
   * The Drulenium entity type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Drulenium entity type label.
   *
   * @var string
   */
  protected $label;

}
